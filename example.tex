\documentclass[%
  twocolumn
%   , hidelinks  % like the hyperref option
   , colorlinks % also like hyperref with CSC colorscheme (this is the default)
%  , monolinks  % colorlinks with one color for all types
%   , hidempi
%   , linenumbers
]{mpi2015-cscpreprint}

%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
% INDIVIDUAL PACKAGES.                                                         %
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%

% Proper hyphenation.
\usepackage[american]{babel}

% Graphical packages.
\usepackage{graphicx}

% Math packages.
\usepackage{amssymb}
\usepackage{amsthm}

\AtBeginDocument{
  \newtheorem{theorem}{Theorem}[section]
  \newtheorem{lemma}[theorem]{Lemma}
}

% The following is just an example, use whichever algroithm package you like
% best
\usepackage[vlined,longend,linesnumbered,ruled]{algorithm2e}

%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
% MAIN DOCUMENT.                                                               %
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%

\begin{document}


%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
% PAPER INFORMATION.                                                           %
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%

\title{CSC Preprint Template Title}

\author[$\ast$]{First Author}
\affil[$\ast$]{Address of first author.\authorcr%
  \email{f.author@email.com}, \orcid{0000-0000-0000-0000}}% chktex 8

\author[$\dagger$]{Second Author}
\affil[$\dagger$]{Address of second author.\authorcr%
  \email{s.author@email.com}, \orcid{0000-0000-0000-0000}}% chktex 8

\shorttitle{Example short title}
\shortauthor{F. Author, S. Author}
\shortdate{}

\keywords{keyword1, keyword2, keyword3}

\msc{MSC1, MSC2, MSC3}

\abstract{%
  This is the preprint template of the Department for Computational Methods
  in Systems and Control Theory (CSC) of the Max Planck Institute for Dynamics
  of Complex Technical Systems, Magdeburg.
  This template is supposed to be the default for all preprints with main or
  corresponding authors at our institute.
  In the following, the use of the template is explained in more details
  with some example for easy use.}

% \novelty{This assesses what the new and significant contribution of this
%   research is.}

\maketitle


%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
% PAPER CONTENT.                                                               %
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%

\section{Introduction}%
\label{sec:intro}

As announced in the abstract, this is the preprint template for the CSC group
of the MPI DCTS, Magdeburg.
In the following sections, we give state guidelines on the preprint commands
and options as well as a few example how to use certain things.
This should not be seen as a general introduction on
``How to write papers with \LaTeX''.
Also, the TeX file corresponding to this document can be used as starting point
for writing your own paper in the CSC preprint style.

In \Cref{sec:pkgcmd}, the most important points, hidden in the class file, are
described, where \Cref{subsec:options} contains main options that can be used
for the template, \Cref{subsec:linkpkg} the link packages used by the
class file, \Cref{subsec:linenumbers} infos on optional line numbering and
\Cref{subsec:meta} the (default) commands for meta information.
\Cref{sec:export} is a small summary of the necessary files for using the
preprint template on other machines and \Cref{sec:other} is then used for
completeness of this file as a minimal example for preprints in this style.

%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%

\section{Packages and commands}%
\label{sec:pkgcmd}

Let's talk here about the options, packages and commands in the template.

%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%

\subsection{Class options}%
\label{subsec:options}

The following is an overview about supported options for the template that can
be given to the document class to change its behavior:

\begin{description}
  \item[twocolumn] Activates the double column style. If not set the
    single column style is the default.
  \item[onecolumn] Counterpart to \texttt{twocolumn}\@. \texttt{onecolumn}
    as option is equivalent to not using \texttt{twocolumn}.
  \item[hidelinks] Turns off the coloring of any link in the paper, i.e., all
    links are printed in default text color.
  \item[hidempi] Hides the MPI related components in the template, i.e.,
    currently the name in the footer.
  \item[linenumbers] Activates line numbering in text.
\end{description}

Loading the algorithm-related packages in the class file is necessary to load
them before the \texttt{cleveref} package is loaded, such that
\texttt{cleverref} can set up its magic appropriately.

As example, to use the preprint in double column style with colored links
you have to call:
\begin{verbatim}
\documentclass[twocolumn]{mpi2015-cscpreprint}
\end{verbatim}
at the beginning of the main TeX file.

%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%

\subsection{Links and hyperref packages}%
\label{subsec:linkpkg}

For links of internal and external kind, the template loads the packages
\texttt{hyperref}, \texttt{url}, \texttt{doi} and \texttt{cleveref}.
This allows the use of classical link commands as \textbackslash\texttt{href},
\textbackslash\texttt{url} and \textbackslash\texttt{doi} if needed and
their use by bibliography styles.
For distinction, the external links are colored in purple, the internal citation
links in red and any internal reference will be given in blue.
Since the coloring of links would also appear in print versions, there is the
option for turning off the coloring; see \Cref{subsec:options}.

For internal referencing, the commands from \texttt{cleveref} are recommended.
The package is preloaded with the \texttt{nameinlink} option such that
names of environments are also part of the link.
Use \verb|\Cref{...}| for any named environment as sections, figures, algorithms
or tables and \verb|\cref{...}| for equations.
By this, the environments are automatically put in by their right naming and
number, e.g., if we want to reference to two sections we use
\begin{verbatim}
  \Cref{sec1,sec2}
\end{verbatim}
which gives could then give
\begin{center}
  \Cref{sec:intro,sec:pkgcmd}
\end{center}
automatically, or for example with the range of three sections
\begin{verbatim}
  \Cref{sec1,sec2,sec3}
\end{verbatim}
to get
\begin{center}
  \Cref{sec:intro,sec:pkgcmd,sec:export}.
\end{center}

%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%

\subsection{Line numbering}%
\label{subsec:linenumbers}

The line numbering of the text that is turned on by the class option
\textbf{\textsf{linenumbers}} uses the \verb|lineno| package.
This has by default problems with math environments especially from other
packages like \verb|amsmath|.
Therefore, a patch is integrated to automatically fix the line numbering for all
math environments from plain \LaTeX{} and the \verb|amsmath| package.
If you want to use the line numbering with other math environments, you can
automatically patch those using our
\begin{verbatim}
  \patchMATHlinenumbers{...}
\end{verbatim}
command for allowing line numbering for your environment, e.g.,
\begin{verbatim}
  \patchMATHlinenumbers{align}
\end{verbatim}
is used in the class file to patch the \verb|align| environment from
\verb|amsmath|.
For other customization of the line numbering, have a look at the package
documentation of the \verb|lineno| package.

%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%

\subsection{Commands for basic paper information}%
\label{subsec:meta}

The template allows you to use a bunch of (default) article commands that should
be used according to this example paper.

\begin{description}
  \item[\normalfont\textbackslash\texttt{title}\{\ldots\}] Defines the title
    of the article.
  \item[\normalfont\textbackslash\texttt{author}{[\ldots]}\{\ldots\}] Defines
    an author of the paper. The optional argument allows to set a footnote mark
    as number or symbol (e.g., 1 or \verb|$\ast$|) to associate authors with
    affiliations. These authors should be given with full first and last names.
    The order of the authors in the TeX document determines the order of the
    authors on the paper.
  \item[\normalfont\textbackslash\texttt{affil}{[\ldots]}\{\ldots\}] Defines
    affiliation for the authors. The optional argument allows to set a footnote
    mark as number or symbol (e.g., 1 or \verb|$\ast$|) to associate authors
    with affiliations. The affiliation should have the following format
\begin{verbatim}
  Address.\authorcr
  \email{...}: , \orcid{...}
\end{verbatim}
    such that email and ORCID are automatically formatted and linked.
  \item[\normalfont\textbackslash\texttt{shorttitle}\{\ldots\}] Is the optional
    running title of the paper printed on all pages starting with 2.
    If no \texttt{shortauthor} is given, it will be printed in the center of
    the page's head.
  \item[\normalfont\textbackslash\texttt{shortauthor}\{\ldots\}] Is the optional
    running authors of the paper printed on all pages starting with 2.
    Here, abbreviated first names should be used. For more then
    3 authors, it's recommended to use the first author abbreviated and followed
    by an ``et al.'' (without quotation marks).% chktex 38
  \item[\normalfont\textbackslash\texttt{shortdate}\{\ldots\}] Controls the
    content of the dates filed in the lower right corner. If empty, the
    compilation date is used in ISO format (year-month-day). Hiding the date
    is also possible by, e.g., using a non-breakable space \verb|\shortdate{|$\sim$\verb|}|.
  \item[\normalfont\textbackslash\texttt{keywords}\{\ldots\}] Comma separated
    list of keywords. If not set, the bold face \textbf{Keywords:} below the
    abstract will not be shown.
  \item[\normalfont\textbackslash\texttt{msc}\{\ldots\}] Comma separated
    list of math subject classification identifiers. If not set, the bold face
    \textbf{AMS subject classification:} below the abstract will not be shown.
    See for example
    \begin{center}
      \url{https://zbmath.org/classification/}
    \end{center}
    for the list of MSC identifiers.
  \item[\normalfont\textbackslash\texttt{abstract}\{\ldots\}] The abstract text
    shown. Note that this is here only a command and not the classical abstract
    environment.
\end{description}


%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%

\section{Export template for preprint server}%
\label{sec:export}

To use the preprint on other machines, e.g., to give it to your co-authors or
for the upload to a preprint server like \emph{arXiv}, the
\texttt{mpi2015-cscpreprint.cls} file needs to be copied wherever needed.
No further files are required, except for basic \LaTeX{} packages, given in any
minimal \emph{TeX Live} or \emph{MiKTeX} installation.


%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%

\section{Other \LaTeX{} stuff}%
\label{sec:other}

Just for the completeness of the template example, here go a few of the usual
stuff you see in other journal styles.
Basically all following points are just reminders of basic \LaTeX{} for paper
writing that also works in the preprint class.

%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%

\subsection{Math environments}%
\label{subsec:math}

For proper referencing of equations, the template loads by default the
\verb|amsmath| package.
The rest can be loaded as desired.
Here, we will just demonstrate to reference equations with the \verb|cleveref|
package.
Given those three equations
\begin{align}
  \label{eqn:eqn1}
  x = a + b,\\
  \label{eqn:eqn2}
  c = \frac{1}{2 x},\\
  \label{eqn:eqn3}
  V = \int\limits_{0}^{\infty} CB \mathrm{d}t.
\end{align}
Then \verb|\cref| should be used instead of \verb|\eqref| for referring to
a single equation~\cref{eqn:eqn1} by
\begin{verbatim}
  \cref{eqn:eqn1}
\end{verbatim}
to a set of equations~\cref{eqn:eqn1,eqn:eqn3}
\begin{verbatim}
  \cref{eqn:eqn1,eqn:eqn3}
\end{verbatim}
and to a range of equations~\cref{eqn:eqn1,eqn:eqn2,eqn:eqn3} by
\begin{verbatim}
  \cref{eqn:eqn1,eqn,eqn:2,eqn:eqn3}.
\end{verbatim}

Do not forget to put an unbreakable space \~{} between the naming and
\verb|\cref|.

\subsection{Math Environments}

To have (numbered) math environments like \emph{Theorems, Definitions, Lemmas},
include the package \verb|amsthm|. To have \verb|\Cref| working properly,
include the definitions of new environments like
\begin{verbatim}
\usepackage{amsthm}

\AtBeginDocument{
  \newtheorem{theorem}{Theorem}[section]
  \newtheorem{lemma}[theorem]{Lemma}
  }
\end{verbatim}
in the preamble. Note the use of the \verb|\AtBeginDocument{}| instruction that
ensures that environments are defined after \emph{cleveref} is loaded.


\begin{lemma}\label{lem:efql}
  Ex falso quod libet.
\end{lemma}

\begin{theorem}\label{thm:efql}
  Ex falso quod libet.
\end{theorem}

From \Cref{lem:efql}, we can infer that \Cref{thm:efql} holds.


%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%

\subsection{Reference lists}%
\label{subsec:ref}

For the bibliography style it is recommended to use something that supports
DOIs.
In this example, we are using the \verb|plainurl| style.
Citations can be done as usual using the \verb|\cite| command.
The template is preloading the \verb|cite| package with the option
\verb|noadjust|.
This allows automatic ordering of references in citation brackets and conversion
to ranges of references if possible.

Citation of a single reference is then~\cite{ref1} with
\begin{verbatim}
  \cite{ref1}
\end{verbatim}
for two references in the wrong order~\cite{ref3, ref1}
\begin{verbatim}
  \cite{ref3,ref1}
\end{verbatim}
and for ranges of references in any order~\cite{ref2, ref1, ref3}
\begin{verbatim}
  \cite{ref2,ref1,ref3}
\end{verbatim}

Remember to adjust the loaded BibTeX file for your stuff and do not forget to
put an unbreakable space \~{} between the naming and \verb|\cite|.
Additionally, the references are added to the table of contents by
\begin{verbatim}
  \addcontentsline{toc}{section}{References}
\end{verbatim}
to give proper links in PDF viewers.

%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%

\subsection{Graphics and TikZ}%
\label{subsec:graphics}

\begin{figure}[t]
  \begin{center}
    \resizebox{8cm}{!}{
    \includegraphics{example-image-a}}

    \caption{Example figure for template.}%
    \label{fig:example}
  \end{center}
\end{figure}

No special mentioning of fancy graphics or use of TikZ here.
Just a simple example how an included graphic should look with the corresponding
\verb|cleveref| reference looks like \Cref{fig:example}.


%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%

\subsection{Tables}%
\label{subsec:tables}

\begin{table}[t]
  \caption{Example table}%
  \label{tab:example}

  \begin{center}
    \begin{tabular}{lc}
      column1 & column2\\
      \hline\noalign{\medskip}
      c1 & c2 \\
      c3 & c4 \\
      \noalign{\medskip}\hline\noalign{\smallskip}
    \end{tabular}
  \end{center}
\end{table}

The same as for figure now with an example table \Cref{tab:example}.

%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%

\subsection{Algorithms}%
\label{subsec:algorithms}

\begin{algorithm}[t]
\KwData{this text}
\KwResult{how to write an CSC preprint}
initialization\;
\While{not at end of this document}{
carefully read current section\;\label{alg:current}
\eIf{understand}{
go to next section\;
current section becomes this one\;
}{
go back to the beginning of current section\;
}
}
\caption{How to use CSC preprint class}\label{alg:one}
\end{algorithm}

This demonstrates algorithms using the {\ttfamily algorithm2e} package. Feel
free to use whichever package you like best, or the preferred journal asks
for. In \Cref{alg:one} the default procedure for learning how to write a CSC
Preprint is addressed. Note how \cref{alg:current} points out that careful
reading is recommended.
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
% *** ACKNOWLEDGEMENTS ***                                                     %
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%

\section*{Acknowledgments}%
\addcontentsline{toc}{section}{Acknowledgments}

Towards the end, there can be an unnumbered section for the acknowledgments,
stating people and organizations who influenced or funded the work in the paper.
Note that this section is just created via
\begin{verbatim}
\section*{Acknowledgments}%
\addcontentsline{toc}{section}{Acknowledgments}
\end{verbatim}


%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
% *** REFERENCES ***                                                           %
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%

\addcontentsline{toc}{section}{References}
\bibliographystyle{plainurl}
\bibliography{exampleref}

\end{document}
